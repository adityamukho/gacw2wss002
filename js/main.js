//Generate a UUID to uniquely identify temporarily created placeholder elements for the lazy load helper.
function uuidv4() { //Courtesy: https://stackoverflow.com/a/2117523/3598131
  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
    const r = Math.random() * 16 | 0, v = (c === 'x') ? r : (r & 0x3 | 0x8);
    return v.toString(16);
  });
}

//Helper to lazy-fetch, compile and render templates on the fly.
Handlebars.registerHelper('load', (name, context = {}) => {
  //Replace all '/' with '-' to get a valid template name.
  const template = name.replace(/\//g, '-');

  //Generate a unique ID for the element that will act as a placeholder for the async fetched and rendered template.
  const elementId = `temp-prefix-${template}-${uuidv4()}`;
  let renderedHTML = false;

  if (!Handlebars.hasOwnProperty('templates')) {
    Handlebars.templates = {};
  }

  //If the template is already loaded, render the template with the context supplied and return immediately. This is
  // a sync operation.
  if (Object.keys(Handlebars.templates).includes(template)) {
    renderedHTML = Handlebars.templates[template](context);
  }

  //Otherwise, fetch the template and render it asynchronously. Since Handlebars helpers are sync functions that are
  // expected to return a rendered element, we return a placeholder element with a unique ID (generated earlier).
  // This is later replaced by the actual contents of the rendered template, whenever available.
  else {
    const path = `/partials/${name}.hbs`;
    $.get(path).done(data => {
      const templateCompiled = Handlebars.compile(data);
      Handlebars.templates[template] = templateCompiled;

      $('div#' + elementId).replaceWith(templateCompiled(context));
    });
  }

  //If the rendered template is ready, return it, otherwise return a span element holding a spinner to serve as a
  // temp placeholder with a unique ID, until the actual content is available.
  return renderedHTML ? new Handlebars.SafeString(renderedHTML) : new Handlebars.SafeString(
    `
        <div id="${elementId}" class="spinner-border spinner-border-sm" role="status">
            <span class="sr-only">Loading...</span>
        </div>
    `);
});

//Helper to create a single object from multiple arguments
// https://stackoverflow.com/a/36362826/3598131
Handlebars.registerHelper('object', ({ hash }) => hash);

//Helper to create a single array from multiple arguments
// https://stackoverflow.com/a/36362826/3598131
Handlebars.registerHelper('array', function() {
  return Array.from(arguments).slice(0, arguments.length - 1);
});

//Takes breadcrumb fragments as input and returns a rendered breadcrumb element. This uses the 'load' helper to
// actually fetch and render render the breadcrumb template (asynchronously if required).
//fragments is an array with successive crumb name and crumb path elements in sequence.
Handlebars.registerHelper('breadcrumbs', (fragments) => {
  const breadcrumbs = [{
    name: 'Home',
    path: '/'
  }];

  //Construct an object from fragments using consecutive elements as key and value repectively.
  for (let i = 0; i < fragments.length; i += 2) {
    breadcrumbs.push({
      name: fragments[i],
      path: (i === fragments.length - 1) ? null : fragments[i + 1] //The last element does not have a path.
    });
  }

  //Delegate to the 'load' helper with the built breadcrumbs context object.
  return Handlebars.helpers.load('breadcrumbs', { breadcrumbs });
});
